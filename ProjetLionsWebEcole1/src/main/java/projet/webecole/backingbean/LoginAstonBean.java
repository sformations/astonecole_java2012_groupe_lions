/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.backingbean;

import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import projet.webecole.model.Adresse;
import projet.webecole.model.LoginInfos;
import projet.webecole.model.Personne;
import projet.webecole.service.EcoleService;

/**
 *
 * @author Administrateur
 */
public class LoginAstonBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginAstonBean.class);
    static final FacesMessage LOGIN_FAILED_FACES_MESSAGE = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "You login action failed !");
    private String login;
    private String password;
    private Personne currentUserPersonne;
    private EcoleService ecoleService;

    public Personne getCurrentUserPersonne() {
        return currentUserPersonne;
    }

    public void setCurrentUserPersonne(Personne currentUserPersonne) {
        this.currentUserPersonne = currentUserPersonne;
    }

    public String getPersonneID() {
        return personneID;
    }

    public void setPersonneID(String personneID) {
        this.personneID = personneID;
    }
    private String personneID;
  

    /**
     * Creates a new instance of LoginAstonBean
     */
    public LoginAstonBean() {
        LOGGER.debug("New LoginAstonBean instance created !");
    }

    public String authenticate() {
        currentUserPersonne  = ecoleService.authentification(login,password);
        if (currentUserPersonne != null) {
            LOGGER.info("{} user has just connected.", login);
            return "LoginCorrect";
        }
        ResourceBundle bundle = ResourceBundle.getBundle("messages.Messages", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String text= bundle.getString("Astonlogin.Loginfailed");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text) );
        login=null;
        return "LoginIncorrect";
    }
   public String disconnect() {
        if(login == null) {
            throw new IllegalStateException("Cannot disconnect an unidentified user");
        }
        LOGGER.info("{} user has just disconnected.", login);
        ((javax.servlet.http.HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
        return "Accueil";
   }
   
   
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public EcoleService getEcoleService() {
        return ecoleService;
    }

    public void setEcoleService(EcoleService ecoleService) {
        this.ecoleService = ecoleService;
    }

}
