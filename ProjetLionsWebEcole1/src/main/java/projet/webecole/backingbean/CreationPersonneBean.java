package projet.webecole.backingbean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import projet.webecole.service.EcoleService;

public class CreationPersonneBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreationPersonneBean.class);
    private String rue;
    private String numéro;
    private String ville;
    private String codepostal;
    private String unePersonnePrenom;
    private String unePersonneNom;
    private String email;
    private String login;
    private String motdespasse;
    private String type = "Default";
    private String datedenaissances;
    private EcoleService ecoleService;

    public CreationPersonneBean() {
        LOGGER.debug("Created new instance of CreationPersonneBean");
    }

    public String ajouter() {
        return ecoleService.creerUser(type, unePersonneNom, unePersonnePrenom, email, numéro, rue, codepostal, ville, login, motdespasse, datedenaissances);


    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getNuméro() {
        return numéro;
    }

    public void setNuméro(String numéro) {
        this.numéro = numéro;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    //les accesseurs
    public String getUnePersonnePrenom() {
        return unePersonnePrenom;
    }

    public void setUnePersonnePrenom(String unePersonnePrenom) {
        this.unePersonnePrenom = unePersonnePrenom;
    }

    public String getUnePersonneNom() {
        return unePersonneNom;
    }

    public void setUnePersonneNom(String unePersonneNom) {
        this.unePersonneNom = unePersonneNom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotdespasse() {
        return motdespasse;
    }

    public void setMotdespasse(String motdespasse) {
        this.motdespasse = motdespasse;
    }

    public String getDatedenaissances() {
        return datedenaissances;
    }

    public void setDatedenaissances(String datedenaissances) {
        this.datedenaissances = datedenaissances;
    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public EcoleService getEcoleService() {
        return ecoleService;
    }

    public void setEcoleService(EcoleService ecoleService) {
        this.ecoleService = ecoleService;
    }

    public String reset() {
        rue = null;
        numéro = null;
        ville = null;
        codepostal = null;
        unePersonnePrenom = null;
        unePersonneNom = null;
        email = null;
        login = null;
        motdespasse = null;
        type = "Default";

        return null;
    }
}
