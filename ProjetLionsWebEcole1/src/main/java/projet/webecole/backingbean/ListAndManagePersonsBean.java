package projet.webecole.backingbean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import projet.webecole.model.Personne;
import projet.webecole.service.EcoleService;

public class ListAndManagePersonsBean implements Validator {

    private static final String EMAIL_REGEXP = "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEXP);
    private static final Logger LOGGER = LoggerFactory.getLogger(ListAndManagePersonsBean.class);
    private DataModel tableUsers;
    private Personne selectedPerson;
    private Personne tempPerson;
    private EcoleService ecoleService;
    private boolean modifyFlag;
    private String typeAffiché = "Admin";

    public ListAndManagePersonsBean() {
    }

    // Toggle d'affichage du formulaire de modification
    public String modifyUser() {
        tempPerson = selectedPerson;
        modifyFlag = true;
        return null;
    }

    // Appels à la couche Service
    public DataModel getTableUsers() {
        if ("Admin".equals(typeAffiché)) {
            tableUsers = new ListDataModel(ecoleService.listerAdmin());
        } else if ("Enseignant".equals(typeAffiché)) {
            tableUsers = new ListDataModel(ecoleService.listerEnseignant());
        } else if ("Etudiant".equals(typeAffiché)) {
            tableUsers = new ListDataModel(ecoleService.listerEtudiant());
        }
        return tableUsers;
    }

    public String deleteUser() {
        ecoleService.supprimerUser(selectedPerson);
        this.selectedPerson = null;
        this.tempPerson = null;
        return null;
    }

    public String commitMods() {
        ecoleService.modifierUser(tempPerson, selectedPerson);
        modifyFlag = false;
        return null;
    }

    // Implementation du Validator
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object emailval) throws ValidatorException {
        {
            String email = (String) emailval;
            Matcher matcher = EMAIL_PATTERN.matcher(email);

            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage();
                message.setDetail("Must be of the form xxx@yyy.zzz");
                message.setSummary("E-mail Addresss not valid");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);
            }
        }
    }

    // Getters and Setters
    public void setTableUsers(DataModel tableUsers) {
        this.tableUsers = tableUsers;
    }

    public Personne getSelectedPerson() {
        return selectedPerson;
    }

    public void setSelectedPerson(Personne selectedP) {
        modifyFlag = false;
        this.selectedPerson = selectedP;
    }

    public void selectPerson(ActionEvent event) {
        setSelectedPerson((Personne) tableUsers.getRowData());
    }

    public EcoleService getEcoleService() {
        return ecoleService;
    }

    public void setEcoleService(EcoleService ecoleService) {
        this.ecoleService = ecoleService;
    }

    public boolean isModifyFlag() {
        return modifyFlag;
    }

    public void setModifyFlag(boolean modifyFlag) {
        this.modifyFlag = modifyFlag;
    }

    public Personne getTempPerson() {
        return tempPerson;
    }

    public void setTempPerson(Personne tempPerson) {
        this.tempPerson = tempPerson;
    }

    public String getTypeAffiché() {
        return typeAffiché;
    }

    public void setTypeAffiché(String typeAffiché) {
        this.typeAffiché = typeAffiché;
    }
}
