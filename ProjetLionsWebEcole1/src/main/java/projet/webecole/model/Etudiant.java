/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

//import java.awt.Image;
import java.util.Calendar;


/**
 *
 * @author Administrateur
 */
public class Etudiant  extends Personne{
   
    //Déclaration des variables
    
    private String idEtudiant;
    
    private Calendar dateNaissance=Calendar.getInstance(); 
    
    //private Date dateNaissance = new Date();
    
    //lien vers la classe Absence
    
    private Absence absence;
    
    //constructeurs
    
    public Etudiant(){
        
         
    }

    public Etudiant(String idEtudiant,Absence absence, String nom,
            String prenom, String email, String photo,String idPersonne,
            Adresse adresse,LoginInfos loginInfos) {
       
        super(nom, prenom, email, photo,idPersonne,adresse,loginInfos);
        this.absence=absence;
        this.idEtudiant=idEtudiant;
    }

   
    
    
    // les accesseurs

    
     public String getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(String idEtudiant) {
        this.idEtudiant = idEtudiant;
    }
    
    public Calendar getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Calendar dateNaissance) {
        this.dateNaissance = dateNaissance;
        
    }

    public Absence getAbsence() {
        return absence;
    }

    public void setAbsence(Absence absence) {
        this.absence = absence;
    }
    
    @Override
    public boolean isOfType(String type) {
       
        if("Etudiant".equals(type)){
        return true;
        } else{
            return false;
        }
        
    }
    
    
}
