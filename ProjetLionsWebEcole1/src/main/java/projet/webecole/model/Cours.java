/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Cours implements Serializable{
    
    //Déclaration des variables
    
    private Date dateCours;
    
    private Date dureeEnMinutes =Calendar.getInstance().getTime();
    
    private String description;
    
    private String idCours;
    
    private String libelle;
    
    //lien vers la classe etudiant
    
    //lien vers la classe matiere
    
    private Matiere matiere;
    
    //lien vers la classe promotion
    
    private Promotion promotion;
    
    //constructeur

    
    public Cours(){
        
    }
    
    public Cours(String  idCours,String libelle, String description,Promotion promotion,Matiere matiere) {
        
        this.idCours=idCours;
        this.libelle=libelle;
        //this.dureeEnMinutes = dureeEnMinutes;
        this.description = description;
        this.matiere=matiere;
        this.promotion=promotion;
        
        dateCours= new Date();
    }
    
    
    //les accessseurs

    public String getIdCours() {
        return idCours;
    }

    public void setIdCours(String idCours) {
        this.idCours = idCours;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    
    public Date getDateCours() {
        return dateCours;
    }

    public void setDateCours(Date dateCours) {
        this.dateCours = dateCours;
    }

    

    public Date getDureeEnMinutes() {
        return dureeEnMinutes;
    }

    public void setDureeEnMinutes(Date dureeEnMinutes) {
        this.dureeEnMinutes = dureeEnMinutes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
    
    
    
}
