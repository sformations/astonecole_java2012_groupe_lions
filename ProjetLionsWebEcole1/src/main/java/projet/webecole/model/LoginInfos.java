/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public class LoginInfos implements Serializable{
    
    // déclaration des variables
    
   
    private String login;
    private String motDePasse;
    
    //constructeurs

    public LoginInfos(){
        
    }
    
    
    public LoginInfos(String login,String motDePasse ) {
        
        this.login = login;
        this.motDePasse = motDePasse;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    
    
    
}
