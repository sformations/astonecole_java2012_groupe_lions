/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */

public class Enseigne implements Serializable{
  
    
    //Déclarations des variables
    private long idEnseigne;
    
    //liens vers la classe promotion, matiere et la classe enseignant
    
    private Promotion promotion;

    private Matiere  matiere;
    
    private Enseignant enseignant;
    //constructreurs
    
    
    public Enseigne(){
        
    }

    
    public Enseigne(long idEnseigne,Enseignant enseignant,Promotion promotion, Matiere matiere) {
        
        this.idEnseigne=idEnseigne;
        this.enseignant=enseignant;
        this.promotion = promotion;
        this.matiere = matiere;
        
    }
    
     // les accesseurs

    public long getIdEnseigne() {
        return idEnseigne;
    }

    public void setIdEnseigne(long idEnseigne) {
        this.idEnseigne = idEnseigne;
    }

    
    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

       
    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }
    
    
     
    
}
