/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

//import java.awt.Image;
import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public  class Personne implements Serializable {
   
    //Déclaration des variables
    
    private String nom;
    
    private String prenom;
    
    private String email;
    
    private String photo;
    
    private String idPersonne;
    
   // lien vers la classe adresse
    
    private Adresse adresse;
    
   //lien vers la classe loginInfos
    
    private LoginInfos loginInfos;
    
    //constructeurs
    
    
    public Personne(){
        
       
    }
    
   

    public Personne(String nom, String prenom, String email, String photo,
            String idPersonne,Adresse adresse,LoginInfos loginInfos) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.photo = photo;
        this.idPersonne=idPersonne;
        this.adresse=adresse;
        this.loginInfos=loginInfos;
    }
    
    // les accesseurs

    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getEmail() {
        return email;
    }

    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(String idPersonne) {
        this.idPersonne = idPersonne;
    }

    
    
    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public LoginInfos getLoginInfos() {
        return loginInfos;
    }

    public void setLoginInfos(LoginInfos loginInfos) {
        this.loginInfos = loginInfos;
    }
   
    public boolean isOfType( String type) {
        return false;
    };
    
}
