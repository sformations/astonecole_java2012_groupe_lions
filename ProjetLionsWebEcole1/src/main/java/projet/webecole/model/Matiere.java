/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public class Matiere implements Serializable{
   
    //Déclaration des variables
    
    private String libelle;
    private long idMatiere;
    
    //Constructeurs
    
    public Matiere(){
        
    }

    public Matiere(String libelle,long idMatiere) {
        this.libelle = libelle;
        this.idMatiere=idMatiere;
    }
    
    
    //les accesseurs

    public String getNLibelle() {
        return libelle;
    }

    public void setNom(String libelle) {
        this.libelle = libelle;
    }

    public long getIdMatiere() {
        return idMatiere;
    }

    public void setIdMatiere(long idMatiere) {
        this.idMatiere = idMatiere;
    }
    
    
    
}
