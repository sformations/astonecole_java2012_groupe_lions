/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

//import java.awt.Image;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Administrateur
 */
public class Enseignant extends Personne {
    
    //déclaration des variables
    
//    private List<Enseigne> listePromotion;
//    private List<Enseigne> listeMatiere;

    private String idEnseignant;

    private Set<Enseigne> listePromotion;
    
    private Set<Enseigne> listeMatiere;
    
    //constructeurs
    
    
    public Enseignant() {
          
    }

    
    public Enseignant(String nom, String prenom, String email, String photo,
         String idPersonne,Adresse adresse,LoginInfos loginInfos,String idEnseignant) {
       
        super(nom, prenom, email, photo,idPersonne,adresse,loginInfos);
        
         this.idEnseignant=idEnseignant;
         
         this.listePromotion = new HashSet<Enseigne>();
        
         this.listeMatiere = new HashSet<Enseigne>();
    }

    
    public String getIdEnseignant() {
        return idEnseignant;
    }

    public void setIdEnseignant(String idEnseignant) {
        this.idEnseignant = idEnseignant;
    }

    @Override
    public boolean isOfType(String type) {
       
        if("Enseignant".equals(type)){
        return true;
        } else{
            return false;
        }
        
    }

    
    
    public Set<Enseigne> getListePromotion() {
        return listePromotion;
    }

    public void setListePromotion(Set<Enseigne> listePromotion) {
        this.listePromotion = listePromotion;
    }

    public Set<Enseigne> getListeMatiere() {
        return listeMatiere;
    }

    public void setListeMatiere(Set<Enseigne> listeMatiere) {
        this.listeMatiere = listeMatiere;
    }
    
    
    
   
     public void addEnseignement(Promotion promotion,Matiere matiere){
        
       // on définie une listeEnseignement de type cours
        
       // List<Cours> listeEnseignement;
       
       // listeEnseignement = new ArrayList<Cours>();
      
       // listeEnseignement.add(new Enseigne(matiere.getNom(),promotion.getLibelle(),enseignant.getEnseignant()));
       
    
    }
}
