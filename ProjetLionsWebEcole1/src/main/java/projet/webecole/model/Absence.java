/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public class Absence implements Serializable{
    
    // Déclarations des variables
    
    private String motif;
    private long idMotif;
   //Constructeur

    public Absence(){
        
    }
    
    public Absence(String motif,long idMotif) {
        this.motif = motif;
        this.idMotif=idMotif;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public long getIdMotif() {
        return idMotif;
    }

    public void setIdMotif(long idMotif) {
        this.idMotif = idMotif;
    }
    
    
    
}
