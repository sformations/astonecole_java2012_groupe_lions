/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrateur
 */
public class Promotion implements Serializable{
    
    //Déclaration des variables
    
    private String libelle;
    
    private long idPromotion;
    

    
    // lien vers la classe Enseigne pour les promotions
  
    private List<Enseigne> listeDesEnseignant;
    
    
    // lien vers la classe Enseigne pour les matieres
   
    private List<Enseigne> listeMatiere;

    // lien vers la classe Cours 
    
    private List<Cours> listeCours;

    //constructeurs

    public Promotion(){
        
    }
    
    public Promotion( long idPromotion, String libelle) {
        this.libelle = libelle;
        this.idPromotion=idPromotion;
    }

    
    //les accesseurs
    
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public long getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(long idPromotion) {
        this.idPromotion = idPromotion;
    }

    
    public List<Enseigne> getListeDesEnseignant() {
        return listeDesEnseignant;
    }

    public void setListeDesEnseignant(List<Enseigne> listeDesEnseignant) {
        this.listeDesEnseignant = listeDesEnseignant;
    }

    public List<Enseigne> getListeMatiere() {
        return listeMatiere;
    }

    public void setListeMatiere(List<Enseigne> listeMatiere) {
        this.listeMatiere = listeMatiere;
    }

    public List<Cours> getListeCours() {
        return listeCours;
    }

    public void setListeCours(List<Cours> listeCours) {
        this.listeCours = listeCours;
    }
    
    
    
}
