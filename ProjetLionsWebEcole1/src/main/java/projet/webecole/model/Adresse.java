/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public class Adresse  implements Serializable{
    
    //déclaration des variables
    
    private long idAdresse;
    
    private String rue;
    
    private String codePostale;
    
    private String ville;
    
    
    
    //constructeur par defaut
    
    public Adresse(){
        
    }

    public Adresse(long idAdresse,String rue, String codePostale, String ville) {
        
        this.idAdresse=idAdresse;
        this.rue = rue;
        this.codePostale = codePostale;
        this.ville = ville;
    }
    
    
    //les accesseurs

    public long getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(long idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
    
    
}
