/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.model;

//import java.awt.Image;

/**
 *
 * @author Administrateur
 */
public class Admin  extends Personne{

    private String idAdmin;
    
    //déclaration des variables
    
    //constructeurs
    
    
    public Admin() {
    }

    public Admin(String idAdmin,String nom, String prenom, String email, String photo,
            String idPersonne,Adresse adresse,LoginInfos loginInfos) {
        super(nom, prenom, email, photo,idPersonne,adresse,loginInfos);
        
        this.idAdmin=idAdmin;
    }

    //les accesseurs
    
    public String getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(String idAdmin) {
        this.idAdmin = idAdmin;
    }
    
    @Override
    public boolean isOfType(String type) {
       
        if("Admin".equals(type)){
        return true;
        } else{
       return false;
        }
        
    }
}
