/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.service;

import java.util.List;
import java.util.ResourceBundle;
import java.util.ServiceLoader;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import projet.webecole.model.Admin;
import projet.webecole.model.Adresse;
import projet.webecole.model.Cours;
import projet.webecole.model.Enseignant;
import projet.webecole.model.Etudiant;
import projet.webecole.model.LoginInfos;
import projet.webecole.model.Personne;

/**
 *
 * @author Administrateur
 */
public interface EcoleService {

//    public static void CreerUser(String rue, String numéro, String modedespasse, String ville, String codepostal, String login, String unePersonneNom, String unePersonnePrenom, String unePersonneId);

    String getAdminIdForLogin(String login);

    Personne getPersonneById(String idPersonne);

    List<Personne> listerEnseignant();

    List<Personne> listerEtudiant();

    List<Personne> listerAdmin();
    
    String creerUser(String type, String nom, String prenom, String email, String numero, String rue, String codepostal, String ville, String login, String mdp, String datedenaissances);
    String supprimerUser(Personne personne);
    void  modifierUser(Personne personneAvant,Personne personneApres);

    List<Cours> listerCours();
    Personne authentification(String login,String motDePasse) ;
    
    
    //int getCours(String idCours);

    final class Provider {

        private Provider() {
        }

        public static EcoleService getDefault() {
            ServiceLoader<EcoleService> ldr = ServiceLoader.load(EcoleService.class);
            for (EcoleService provider : ldr) {
                return provider;
            }
            throw new IllegalStateException("No EcoleService registered");
        }
    }
}
