/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projet.webecole.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import projet.webecole.model.Admin;
import projet.webecole.model.Adresse;
import projet.webecole.model.Cours;
import projet.webecole.model.Enseignant;
import projet.webecole.model.Etudiant;
import projet.webecole.model.LoginInfos;
import projet.webecole.model.Personne;

/**
 *
 * @author Administrateur
 */
public class EcoleServiceStub implements EcoleService {

    //private static final List<Personne> PERSONNES = new ArrayList<Personne>();
//    private static final Map<String, List<? extends Personne>> PERSONNES = new TreeMap<String, List<? extends Personne>>();
//    private static final String Admin_ONE_ID = "Admin1";
//    private static final String Admin_TWO_ID = "Admin2";
//    private static final String Enseignant_ONE_ID = "Enseignant1";
//    private static final String Etudiant_ONE_ID = "Etudiant1";
    private static final List<Personne> testList = new ArrayList<Personne>();

    static {

        testList.add(new Admin("001", "Foussat", "Cyril", "cf@w.fr", "Maface.jpg", "Admin1",
                new Adresse(75, "Champs Elysées", "75008", "Paris"), new LoginInfos("Cyril", "mdp")));
        testList.add(new Admin("002", "Keiti", "Alain", "ak@f.fr", "Chapeau.jpg", "Admin2",
                new Adresse(101, "Champs Elysées", "75008", "Paris"), new LoginInfos("Alain", "pw")));
        testList.add(new Enseignant("Vengatesan", "Prassana", "pv@g.fr", "Sushi.jpg", "Prof1",
                new Adresse(12, "Champs Elysées", "75008", "Paris"), new LoginInfos("Prassana", "pr"), "P001"));
        testList.add(new Etudiant("E001", null, "Lennon", "John", "jl@heaven.com", "Johnny.jpg", "Etu1",
                new Adresse(1, "Trafalgar Square", "666", "Hell"), new LoginInfos("John", "Lennon")));
    }

//    static {
//        PERSONNES.put(
//                Admin_ONE_ID,
//                neArrays.asList(
//                new Admin(Admin_ONE_ID, "MARTIN", "David", "dmartin@hotmail.com",
//                "photo.jpg", "numero 1", new Adresse(41, "Avenue Geoerges CLEMENCEAUX ", "92240", "Malakoff"), new LoginInfos("Admin1", "admin1")),
//                new Admin(Admin_TWO_ID, "DUPON", "jean", "dmartin@hotmail.com",
//                "photo.jpg", "numero 1", new Adresse(41, "Avenue Geoerges CLEMENCEAUX ", "92240", "Malakoff"), new LoginInfos("Admin1", "admin1"))));
//        PERSONNES.put(
//                Enseignant_ONE_ID,
//                Arrays.asList(
//                new Enseignant(),
//                new Enseignant()));
//        PERSONNES.put(
//                Etudiant_ONE_ID,
//                Arrays.asList(
//                new Etudiant(),
//                new Etudiant(),
//                new Etudiant()));
//        }
    @Override
    public String getAdminIdForLogin(String login) {
//           
//           if("Admin1".equals(login)){
//               
//               return Admin_ONE_ID;
//           }
//           
//           if("Admin2".equals(login)){
//               
//               return Admin_TWO_ID;
//           }
//           
//           if("Enseignant1".equals(login)){
//               
//               return   Enseignant_ONE_ID;
//           }
//           
//           
//           if("Etudiant1".equals(login)){
//               
//              return  Etudiant_ONE_ID;
//           }
//         
        return null;
    }

    @Override
    public Personne getPersonneById(String idPersonne) {
//        for (Map.Entry<String, List<? extends Personne>> pers : PERSONNES.entrySet()) {
//            List<? extends Personne> list = pers.getValue();
//            for (Personne personne : list) {
//                if (personne.getIdPersonne().equals(idPersonne)) {
//                    return personne;
//                }
//            }
//        }
        return null;
    }

    @Override
    public List<Personne> listerAdmin() {

        List<Personne> allAdmins = new ArrayList<Personne>();

        for (Personne personne : testList) {
            if (personne.isOfType("Admin")) {

                allAdmins.add(personne);

            }

        }

        return allAdmins;
    }
//   

    @Override
    public List<Personne> listerEnseignant() {
        List<Personne> allEnseignant = new ArrayList<Personne>();

        for (Personne personne : testList) {
            if (personne.isOfType("Enseignant")) {

                allEnseignant.add(personne);

            }

        }

        return allEnseignant;
    }

    @Override
    public List<Personne> listerEtudiant() {
        List<Personne> allEtudiant = new ArrayList<Personne>();

        for (Personne personne : testList) {
            if (personne.isOfType("Etudiant")) {

                allEtudiant.add(personne);
            }
        }
        return allEtudiant;
    }

    @Override
    public String creerUser(String type, String nom, String prenom, String email, String numero, String rue, String codepostal, String ville, String login, String mdp, String datedenaissances) {
        if ("Admin".equals(type)) {
            testList.add(new Admin("Admin" + testList.size(), nom, prenom, email, nom + ".jpg", "User" + testList.size(),
                    new Adresse(Integer.parseInt(numero.trim()), rue, codepostal, ville), new LoginInfos(login, mdp)));
            return "Success";
        } else if ("Enseignant".equals(type)) {
            testList.add(new Enseignant(nom, prenom, email, nom + ".jpg", "User" + testList.size(),
                    new Adresse(Integer.parseInt(numero.trim()), rue, codepostal, ville), new LoginInfos(login, mdp), "Prof" + testList.size()));
            return "Success";
        } else if ("Etudiant".equals(type)) {
            testList.add(new Etudiant("Etu" + testList.size(), null, nom, prenom, email, nom + ".jpg", "User" + testList.size(),
                    new Adresse(Integer.parseInt(numero.trim()), rue, codepostal, ville), new LoginInfos(login, mdp)));
            return "Success";
        }

        return "Fail";
    }

    @Override
    public String supprimerUser(Personne personne) {

        testList.remove(personne);

        return "success";
    }

    @Override
    public void modifierUser(Personne personneAvant, Personne personneApres) {

        testList.set(testList.indexOf(personneAvant), personneApres);

    }

    @Override
    public List<Cours> listerCours() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Personne authentification(String login, String motDePasse) {
        for (Personne personne : testList) {
            if (personne.getLoginInfos().getLogin().equals(login) && personne.getLoginInfos().getMotDePasse().equals(motDePasse)) {
                return personne;
            }
        }
        return null;
    }
}
